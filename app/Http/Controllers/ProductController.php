<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Models\Tag;
use GuzzleHttp\Psr7\Query;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Image;
use PDF;

class ProductController extends Controller
{
    public function index()
    {
        // $products = Product::all();
        $products = Product::orderBy('id', 'desc')->get();
        return view('backend.products.index', compact('products'));
    }

    public function create()
    {
        $categories = Category::pluck('title', 'id')->toArray();
        $tags = Tag::pluck('title', 'id')->toArray();
        return view('backend.products.create', compact('categories', 'tags'));
    }

    public function store(ProductRequest $request)
    {
        try {
            $requestData = $request->all();

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                Image::make($request->file('image'))
                    ->resize(300, 200)
                    ->save(storage_path() . '/app/public/products/' . $fileName);
                $requestData['image'] = $fileName;
            }

            $requestData['created_by'] = auth()->user()->id;
            $product = Product::create($requestData);
          
            $product->tags()->attach($request->tag_ids);

            // Session::flush('message', 'Successfully Saved');
            // return redirect()->route('products.index')->with('message','Successfully Saved !');
            return redirect()->route('products.index')->withMessage('Successfully Saved !');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Product $product) //dependency injection
    
    {

        // dd($product);
        $this->authorize('product-show', $product);
        // $product = Product::where('id', $id)->first();
        // $product = Product::where('id', $id)->firstOrFail();

        // $product = Product::findOrFail($product);

        // return view('backend.products.show', [
        //     'product' => $product
        // ]);

        return view('backend.products.show', compact('product'));
    }

    public function edit(Product $product)
    {
        $this->authorize('product-edit', $product);

        $categories = Category::pluck('title', 'id')->toArray();
        // $product = Product::findOrFail($id);
        return view('backend.products.edit', compact('product', 'categories'));
    }

    public function update(ProductRequest $request, Product $product)
    {
        try {
            $requestData = $request->all();

            // $product = Product::findOrFail($id);

            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = time() . '.' . $file->getClientOriginalExtension();
                Image::make($request->file('image'))
                    ->resize(300, 200)
                    ->save(storage_path() . '/app/public/products/' . $fileName);
                $requestData['image'] = $fileName;
            } else {
                $requestData['image'] = $product->image;
            }

            $requestData['updated_by'] = auth()->id();
            $product->update($requestData);

            return redirect()->route('products.index')->withMessage('Successfully Updated !');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Product $product)
    {
        $this->authorize('product-delete', $product);
        // $product = Product::findOrFail($id);
        $product->update(['deleted_by'=>auth()->id()]);

        $product->delete();

        return redirect()->route('products.index')->withMessage('Successfully Deleted !');
    }

    public function trash()
    {
        $this->authorize('product-trash-list');

        $products = Product::onlyTrashed()->get();
        return view('backend.products.trash', compact('products'));
    }

    public function restore($id)
    {
        Product::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect()->route('products.trash')->withMessage('Successfully Restored !');
    }

    public function delete($id)
    {
        Product::withTrashed()
            ->where('id', $id)
            ->forceDelete();

        return redirect()->route('products.trash')->withMessage('Deleted Successfully');
    }

    public function pdf()
    {
        $products = Product::latest()->get();
        $pdf = PDF::loadView('backend.products.pdf', compact('products'));
        return $pdf->download('products.pdf');
    }



}