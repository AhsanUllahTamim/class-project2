<x-backend.layouts.master>
    <h1 class="mt-4">Products</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">Products</li>
    </ol>

    <div class="card mb-4">
        <div class="card-header">
            <i class="fas fa-table me-1"></i>
            Product Details
            <a class="btn btn-sm btn-primary" href="{{ route('products.index') }}">List</a>
        </div>
        <div class="card-body">
            <h3>Category: {{ $product->category->title ?? 'N/A' }}</h3>
            <h3>Title: {{ $product->title }}</h3>
            <p>Description: {{ $product->description }}</p>
            <p>Price: {{ $product->price }}</p>
            <img src="{{ asset('storage/products/'. $product->image) }}" />
            <p>Created At: {{ $product->created_at->diffForHumans() }} By: {{$product->createdBy->name??'N/A'}}</p>
            <p>Updated At: {{ $product->updated_at->diffForHumans() }} By: {{$product->updatedBy->name??'N/A'}}</p>
        </div>

    </div>
</x-backend.layouts.master>