<x-backend.layouts.master>
    <h1 class="mt-4">Products</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
        <li class="breadcrumb-item active">products</li>
    </ol>

    <div class="card mb-4">
       
        <div class="card-body">

           

            <table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Order No</th>
                        <th>Status</th>
                        <th>phone_no</th>
                        <th>email</th>
                        <th>shipping_address</th>
                        <th>payment_method</th>
                        <td>Action</td>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($orders as $order)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $order->order_no }}</td>
                        <td>{{ $order->status}}</td>
                        <td>{{ $order->phone_no}}</td>
                        <td>{{ $order->email}}</td>
                        <td>{{ $order->shipping_address}}</td>
                        <td>{{ $order->payment_method}}</td>
                        <td>
                        <a class="btn btn-info btn-sm" href="{{route('orders.details', $order->id)}}">ShowOrderDetails</a> 
                         <a class="btn btn-danger btn-sm" href="">remove</a>
                        <a class="btn btn-warning btn-sm" href="{{ route('orders.pdf')}}">PDF</a>
                        </td>
                        

                           
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</x-backend.layouts.master>