<x-backend.layouts.master>

    <h1>order  Details</h1>

<table id="datatablesSimple">
    <thead>
        <tr>
            <th>SL</th>
            <th>Product Id</th>
            <th>Order Id</th>
            <th>Product Title</th>
            <th>Unit Price</th>
            <th>Quentity</th>
        </tr>
    </thead>

    <tbody>

        @foreach ($orderdetails as $orderdetail)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $orderdetail->product_id }}</td>
            <td>{{ $orderdetail->order_id }}</td>
            <td>{{ $orderdetail->product_title}} TK</td>
            <td>{{ $orderdetail->unit_price ?? 'N/A' }}</td>
            <td>{{ $orderdetail->qty }}</td>
           
        </tr>
        @endforeach

    </tbody>
</table>

</x-backend.layouts.master>