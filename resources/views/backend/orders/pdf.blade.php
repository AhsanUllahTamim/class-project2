<h1>Order List</h1>


<p>Name:  {{ Auth::user()->name }} </p>

<table id="datatablesSimple">
                <thead>
                    <tr>
                        <th>SL</th>
                        <th>Order No</th>
                        <th>Status</th>
                        <th>phone_no</th>
                        <th>email</th>
                        <th>shipping_address</th>
                        <th>payment_method</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach ($orders as $order)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $order->order_no }}</td>
                        <td>{{ $order->status}}</td>
                        <td>{{ $order->phone_no}}</td>
                        <td>{{ $order->email}}</td>
                        <td>{{ $order->shipping_address}}</td>
                        <td>{{ $order->payment_method}}</td>
                                  
                    </tr>
                    @endforeach

                </tbody>
            </table>