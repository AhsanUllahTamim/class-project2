<x-frontend.layouts.master>


    <div class="bg-light p-5 rounded">
        <div class="card">
            <div class="card-header"></div>
            <div class="card-body">
            <form method="post" action="{{route('orders.create')}}">
                    @csrf
                <table class="table">
                    <thead>
                        <tr>
                            <th>#SL</th>
                            <th>Product</th>
                            <th>Qty</th>
                            <th>Unit Price</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                        $itemTotalPrice = 0;
                        $totalPrice = 0;
                        @endphp
                        @foreach ($cartItems as $cart)


                        @php
                        $itemTotalPrice = $cart->qty * $cart->unit_price;
                        $totalPrice += $itemTotalPrice;
                        @endphp

                        <tr>
                            <td>
                                {{ $loop->iteration }}
                                <input type="hidden" name="product_ids[]" value="{{$cart->product->id}}" />
                            </td>
                            <td>{{ $cart->product->title }}</td>
                            <td>
                                <input type="number" name="product_qty[]" value="{{$cart->qty}}" />
                            </td>
                            <td>{{ $cart->unit_price }}</td>
                            <td>{{ $itemTotalPrice }}</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td align="right" colspan="4">Total Price</td>
                            <td>{{ $totalPrice }}</td>

                        </tr>
                    </tbody>
                </table>

             
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email</label>
                        <input type="email" name="email" class="form-control" id="exampleInputEmail1">
                    </div>
                    <div class="mb-3">
                        <label for="phone" class="form-label">Phone</label>
                        <input type="text" name="phone" class="form-control" id="phone" >
                    </div>
                    <div class="mb-3">
                        <label for="shipping_address" class="form-label">Address</label>
                        <textarea name="shipping_address" id="shipping_address" class="form-control"></textarea>
                    </div>
                    <button class="btn btn-primary" type="submit">Place Order</button>
                </form>
            </div>
        </div>
    </div>
</x-frontend.layouts.master>