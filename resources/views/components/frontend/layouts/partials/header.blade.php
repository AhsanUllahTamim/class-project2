<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{ url('/') }}">E-Shop</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="{{ url('/') }}">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">About</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Categories
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">

            @foreach ($categories as $category)
            <a class="dropdown-item" href="{{ route('category-wise-product', $category->id) }}">{{$category->title}}</a>
        </li>
        @endforeach
      </ul>
      </li>
      <li class="nav-item">

      <a href="{{ route('shoping-bag') }}"
        <button type="button" class="btn btn-primary position-relative">
          Cart
          <span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-info">
            {{ $totalItemsInCart }}
          </span>
        </button>
      </a>

      </li>
      </ul>
      @guest
      <a href="{{ route('login') }}" class="btn btn-info">Login</a>
      <a href="{{ route('register') }}" class="btn btn-primary">Register</a>
      @else
      <form method="POST" action="{{ route('logout') }}">
        @csrf

        <a class="btn btn-info" href="route('logout')" onclick="event.preventDefault();
                            this.closest('form').submit();">
          {{ __('Log Out') }}
        </a>
      </form>
      @endguest

    </div>
  </div>
</nav>